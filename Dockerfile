# FROM node:lts-alpine
# WORKDIR /usr/src/app
# COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
# RUN npm install
# COPY . .
# EXPOSE 3000
# RUN chown -R node /usr/src/app
# USER node
# CMD ["npm", "start"]

# RUN mkdir -p /app
#     WORKDIR /app
#     COPY package.json /app
#     RUN npm install
#     COPY . /app
#     RUN npm run build --prod

#     FROM nginx:1.20.1
#     COPY --from=build-step /app/dist/my-app /usr/share/nginx/html
#     EXPOSE 4200:80

# base image
FROM node:alpine
# create & set working directory for next app
RUN mkdir -p /usr/src
WORKDIR /usr/src
# copy source files next app
COPY . /usr/src
COPY package.json ./
# install dependencies for next app
RUN npm install
# start Next app
RUN npm run build
EXPOSE 3000
CMD npm run start

